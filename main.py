from fastapi import FastAPI
import random
import string
from typing import Dict, Any


from intents.CambiaScenarioIntent import CambiaScenarioIntent
from intents.CreaMeetingIntent import CreaMeetingIntent
from intents.CreaStanzaIntent import CreaStanzaIntent
from intents.Intent import Intent
from intents.recognition.IntentRecognition import IntentRecognition
from intents.recognition.IntentType import IntentType
from intents.TokenValori import TokenValori
from translation.Translation import Translation
from translation.TranslationType import TranslationType

app = FastAPI()


def generare_stringa_random(n):
    caratteri = string.ascii_letters + string.digits
    return ''.join(random.choice(caratteri) for _ in range(n))


mapIntent = {}


@app.get("/api/translation")
async def do_translation(params: Dict[str, Any]):
    if "frase" in params and "lang" in params and "lang_translation" in params:
        lang_frase_str = params["lang"]
        lang_traduzione_str = params["lang_translation"]
        frase = params["frase"]
        lang_frase = TranslationType.ottieni_enum(lang_frase_str)
        lang_traduzione = TranslationType.ottieni_enum(lang_traduzione_str)

        if lang_frase is not None and lang_traduzione is not None:
            tradModel = Translation(lang_frase,lang_traduzione)
            traduzione = tradModel.traduci(frase)
            return {
                "Ris": 1,
                "Mess": "Fatto",
                "Traduzione": traduzione
            }
        else:
            return {
                "Ris": 0,
                "Mess": "Lingue non supportate"
            }
    else:
        return {
            "Ris": -1,
            "Mess": "Parametri errati"
        }


@app.post("/api/message")
async def do_conversation(params: Dict[str, Any]):
    if "frase" in params:
        frase = params["frase"]
        tipo = IntentRecognition.get_tipo(frase)
        idConversazione = params["idConversazione"] if "idConversazione" in params else ""

        print(f"Tipo frase: ${tipo}")
        if tipo == IntentType.NON_CAPITO:
            print("intento non capito")
            json = {
                "Ris": 1,
                "Mess": "Fatto",
                "Conversazione": {"type": IntentType.NON_CAPITO,
                                  "id_conversazione": idConversazione}
            }
            return json
        if "idConversazione" in params:
            chiave = params["idConversazione"]
            if chiave in mapIntent:
                conversazione = mapIntent[chiave]
            else:
                return {
                    "Ris": -1,
                    "Mess": "Chiave errata"
                }
        else:
            while True:
                chiave = generare_stringa_random(64)
                if chiave not in mapIntent:
                    break
            if tipo == IntentType.CREA_MEETING:
                conversazione = CreaMeetingIntent(chiave)
            elif tipo == IntentType.CREA_STANZA:
                conversazione = CreaStanzaIntent(chiave)
            else:
                conversazione = CambiaScenarioIntent(chiave)
        prediction, model_output = conversazione.predict(frase)
        result = prediction[0]

        if tipo == IntentType.CREA_MEETING:
            tmpConv = CreaMeetingIntent("-1")
        elif tipo == IntentType.CREA_STANZA:
            tmpConv = CreaStanzaIntent("-1")
        else:
            tmpConv = CambiaScenarioIntent("-1")
        for item in result:
            for key, value in item.items():
                if value != 'O':
                    token = TokenValori(value, key)
                    tmpConv.aggiungi_intent_dichiarato(token)
        final_conv = Intent.merge_conversation(conversazione, tmpConv)
        mapIntent[chiave] = final_conv

        if len(final_conv.ottieni_intent_non_dichiarati()) == 0:
            final_conv.aggiorna_id_conversazione("")
            del mapIntent[chiave]

        json = {
            "Ris": 1,
            "Mess": "Fatto",
            "Conversazione": final_conv
        }
        if "debug_mode" in params:
            debug = params["debug_mode"]
            if debug:
                json["Debug"] = result
        return json
    else:
        return {
            "Ris": -1,
            "Mess": "Parametri errati"
        }
