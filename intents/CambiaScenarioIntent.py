from intents.Intent import Intent

import pickle
import multiprocessing

from intents.recognition.IntentType import IntentType

multiprocessing.freeze_support()
model = pickle.load(open('models/model_CambioScenario.pkl', 'rb'))


class CambiaScenarioIntent(Intent):
    def __init__(self, id_conversazione):
        intent_necessari = {"B_NOME_SCENARIO": "Nome scenario"}
        super().__init__(IntentType.CAMBIO_SCENARIO,id_conversazione, intent_necessari)

    def predict(self, frase):
        return model.predict([frase])
