from intents.Intent import Intent

import pickle
import multiprocessing

from intents.recognition.IntentType import IntentType

multiprocessing.freeze_support()
model = pickle.load(open('models/model_SchedulaMeeting.pkl', 'rb'))


class CreaMeetingIntent(Intent):
    def __init__(self, id_conversazione):
        intent_necessari = {"B_NOME-MEETING": "Nome", "B_GIORNO": "Giorno",
                            "B_ORA-INIZ": "Ora Inizio"}
        super().__init__(IntentType.CREA_MEETING,id_conversazione, intent_necessari)

    def predict(self, frase):
        return model.predict([frase])
