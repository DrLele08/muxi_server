from intents.Intent import Intent

import pickle
import multiprocessing

from intents.recognition.IntentType import IntentType

multiprocessing.freeze_support()
model = pickle.load(open('models/model_CreaStanza.pkl', 'rb'))


class CreaStanzaIntent(Intent):
    def __init__(self, id_conversazione):
        intent_necessari = {"B_TIPO_SCEN":"Scenario", "B_NUM_PART":"Numero partecipanti", "B_NOME":"Nome scenario"}
        super().__init__(IntentType.CREA_STANZA,id_conversazione, intent_necessari)

    def predict(self, frase):
        return model.predict([frase])
