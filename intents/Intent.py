class Intent:
    def __init__(self,type, id_conversazione, intent_necessari):
        self.type = type
        self.id_conversazione = id_conversazione
        self.intent_dichiarati = {}
        self.intent_necessari = intent_necessari
        self.intent_non_dichiarati = self.intent_necessari.copy()

    def predict(self,frase):
        pass

    def aggiungi_intent_dichiarato(self, intent):
        val = intent.valore
        for (key, value) in self.intent_dichiarati.items():
            if key == intent.token:
                val = f"{value} {val}"
        self.intent_dichiarati[intent.token] = val
        if intent.token in self.intent_non_dichiarati:
            del self.intent_non_dichiarati[intent.token]

    def ottieni_id_conversazione(self):
        return self.id_conversazione

    def aggiorna_id_conversazione(self,id_conversazione):
        self.id_conversazione=id_conversazione

    def ottieni_intent_dichiarati(self):
        return self.intent_dichiarati

    def ottieni_intent_non_dichiarati(self):
        return self.intent_non_dichiarati

    @staticmethod
    def merge_conversation(finalConv, tmpConv):
        for (key, value) in tmpConv.intent_dichiarati.items():
            finalConv.intent_dichiarati[key] = value
            if key in finalConv.intent_non_dichiarati:
                del finalConv.intent_non_dichiarati[key]
        return finalConv
