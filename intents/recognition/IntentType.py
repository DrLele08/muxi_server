from enum import Enum


class IntentType(Enum):
    CREA_STANZA = 0
    CREA_MEETING = 1
    CAMBIO_SCENARIO = 2
    NON_CAPITO = 3
