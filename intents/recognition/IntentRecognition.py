from intents.recognition.IntentType import IntentType


from torch import nn

import multiprocessing
import os
import torch
from transformers import BertTokenizer

from intents.recognition.SentimentClassifier import SentimentClassifier

PRE_TRAINED_MODEL_NAME = 'bert-base-cased'

class IntentRecognition:
    @staticmethod
    def get_tipo(frase):
        multiprocessing.freeze_support()
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        class_names = [0, 1, 2]

        MAX_LEN = 160

        tokenizer = BertTokenizer.from_pretrained(PRE_TRAINED_MODEL_NAME)

        multiprocessing.freeze_support()
        model = SentimentClassifier(len(class_names))
        model_path = './intents/recognition/best_model_state.bin'
        if os.path.exists(model_path):
            model.load_state_dict(torch.load(model_path, map_location=device), strict=False)
            model.to(device)
            model.eval()
        else:
            print("Errore: file di stato del modello non trovato.")

        encoded_review = tokenizer.encode_plus(
            frase,
            max_length=MAX_LEN,
            add_special_tokens=True,
            return_token_type_ids=False,
            padding='max_length',  # Utilizza il parametro padding invece di pad_to_max_length
            return_attention_mask=True,
            return_tensors='pt',
            truncation=True  # Aggiungi questo parametro per evitare il FutureWarning
        )

        # Converti gli oggetti in tensori PyTorch senza clone() e detach()
        input_ids = encoded_review['input_ids'].to(device)
        attention_mask = encoded_review['attention_mask'].to(device)

        with torch.no_grad():
            output = model(input_ids, attention_mask)

        _, prediction = torch.max(output, dim=1)

        tipo = class_names[prediction]

        # Calcola la precisione
        softmax_output = torch.nn.functional.softmax(output, dim=1)
        confidence, _ = torch.max(softmax_output, dim=1)

        print(confidence)
        # Applica la condizione
        if confidence.item() > 0.75:
            if tipo == 0:
                return IntentType.CREA_STANZA
            elif tipo == 1:
                return IntentType.CREA_MEETING
            else:
                return IntentType.CAMBIO_SCENARIO
        else:
            return IntentType.NON_CAPITO