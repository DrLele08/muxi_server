from enum import Enum


class TranslationType(Enum):
    IT = 1
    EN = 2

    @staticmethod
    def ottieni_enum(nome):
        for tipo in TranslationType:
            if tipo.name == nome:
                return tipo
        return None
