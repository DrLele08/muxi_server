from transformers import pipeline

from translation.TranslationType import TranslationType

pipe_en_to_it = pipeline("translation", model="Helsinki-NLP/opus-mt-en-it")
pipe_it_to_en = pipeline("translation", model="Helsinki-NLP/opus-mt-it-en")


class Translation:
    def __init__(self, actual_lang, trans_lang):
        self.actual_lang = actual_lang
        self.trans_lang = trans_lang

    def ottioni_actual_lang(self):
        return self.actual_lang

    def ottieni_trans_lang(self):
        return self.trans_lang

    def traduci(self, frase):
        if self.actual_lang == TranslationType.IT and self.trans_lang == TranslationType.EN:
            return pipe_it_to_en(frase)[0]["translation_text"]
        elif self.actual_lang == TranslationType.EN and self.trans_lang == TranslationType.IT:
            return pipe_en_to_it(frase)[0]["translation_text"]
